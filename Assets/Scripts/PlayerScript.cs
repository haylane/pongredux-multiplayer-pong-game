﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public Vector2 speed = new Vector2(0, 10);
	private Vector2 movement;

	private float halfYself;
	private float posY;
	
	// Use this for initialization
	void Start () {
		halfYself = gameObject.renderer.bounds.extents.y;
	}
	
	// Update is called once per frame
	void Update () {
		float inputY = Input.GetAxis ("Vertical");
		float posY = gameObject.transform.position.y;
		//Debug.Log(string.Format("inputY   {0}", inputY));
		if ((((posY - halfYself) < - 2.85) && inputY < 0) || (((posY + halfYself) > 2.85) && inputY > 0)){
			movement = new Vector2 (0, 0);
			//transform.position = new Vector2(transform.position.x, transform.position.y + 0.1f);
		}
		else
		 movement = new Vector2 (0, speed.y * inputY);
	}

	void FixedUpdate()
	{
		rigidbody2D.velocity = movement;
	}

	void OnTriggerEnter2D(Collider2D otherCollider){
		audio.Play ();
		//networkView.RPC ("ScoreUpdatePlayer2", RPCMode.AllBuffered, null);
		//transform.position = new Vector2 (0, 0);
	}
}
