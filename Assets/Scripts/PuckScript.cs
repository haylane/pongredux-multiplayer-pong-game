﻿using UnityEngine;
using System.Collections;

public class PuckScript : MonoBehaviour
{

		//public Vector2 speed = new Vector2(1,1);
		public Texture pitch;
		public Transform pitchTransform;
		private GameObject statObject;
	 

		//private Vector2 spritePos;
		private Vector2 movement;
		public Vector2 velocity;
		private float x;
		private float y;
		float halfXself, halfYself;
		float posX, posY;
		public float player1Score;
		public float player2Score;
		
		void Awake ()
		{
				player1Score = 0;
				player2Score = 0;
		}

		void Start ()
		{
	
				while (x == 0)
						x = 5 * Random.Range (-1.0f, 1.1f);
				while (y == 0)
						y = 5 * Random.Range (-1.0f, 1.1f);
				halfXself = gameObject.renderer.bounds.extents.x;
				halfYself = gameObject.renderer.bounds.extents.y;
				movement = new Vector2 (x, y);

	 
		}
	
		// Update is called once per frame
		void Update ()
		{
				CollisionWithPitch ();
		}

		void CollisionWithPitch ()
		{

				float posX = gameObject.transform.position.x;
				float posY = gameObject.transform.position.y;
				if ((posY - halfYself) < - 2.85) {
						//velocity.y *= -1;
						movement.y *= -1;
						transform.position = new Vector2 (transform.position.x, transform.position.y + 0.1f);
				}
				if ((posY + halfYself) > 2.85) {
						movement.y *= -1;
						transform.position = new Vector2 (transform.position.x, transform.position.y - 0.1f);
				}
				if ((posX - halfXself) < -3.85) {
						movement.x *= -1;
						transform.position = new Vector2 (transform.position.x + 0.1f, transform.position.y);
				}
				if ((posX + halfXself) > 3.85) {
						//velocity.x *= -1;
						movement.x *= -1;
						transform.position = new Vector2 (transform.position.x - 0.1f, transform.position.y);
				}
		}

		void FixedUpdate ()
		{
				rigidbody2D.velocity = movement;
		}

		void OnTriggerEnter2D (Collider2D otherCollider)
		{
				statObject = GameObject.Find ("GameStats(Clone)");
				if (statObject == null)
						Debug.Log ("No statObject");
				movement.x *= -1;
				if (otherCollider.tag == "Player1") { 
						networkView.RPC ("incPlayer1Score", RPCMode.AllBuffered);
						//player1Score++;
//						statObject.SendMessage ("ScoreUpdatePlayer1");
				}
				else if (otherCollider.tag != "Player1") {
						networkView.RPC ("incPlayer2Score", RPCMode.AllBuffered);
						//player2Score++;
//						statObject.SendMessage ("ScoreUpdatePlayer2");
				}
		}

		void OnDisconnectedFromServer ()
		{
				Destroy (gameObject);
		}

		[RPC]
		void incPlayer1Score ()
		{
				player1Score++;
		}

		[RPC]
		void incPlayer2Score ()
		{
				player2Score++;
		}

		void OnGUI ()
		{
				string foo = player1Score.ToString ();
				GUI.contentColor = Color.green; 
				GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 20, 40), foo);
				foo = player2Score.ToString ();
				GUI.contentColor = Color.red; 
				GUI.Label (new Rect (Screen.width / 2, Screen.height / 2 + 20, 20, 40), foo);
		}
}
